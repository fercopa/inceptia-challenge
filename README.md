# inceptia-challenge

## Python version
Python 3.8.17

## Como usar
Clonar el repositorio

```sh
git clone https://gitlab.com/fercopa/inceptia-challenge.git myproj
cd myproj
```

Creamos un entorno virtual e instalamos los requirements

```sh
python -m venv venv
pip install -r requirements.txt
```

### Recomendación
Agregamos a la variable de entorno `PYTHONPATH` nuestro proyecto

```sh
export PYTHONPATH=$PYTHONPATH:path/to/myproj
```

## Ejercicio 1
La función `is_hot_in_pehuajo` solicitada por el bot se encuentra en `app/geoapi.py`

### Modo de uso
```python
from app.geoapi import GeoAPI
GeoAPI.is_hot_in_pehuajo()
```

## Ejercicio 2.1
La función `is_product_available` se encuentra en `app/product_handler`

### Mode de uso
```python
from app.product_handler import is_product_available
is_product_available("limon", 1)
```

## Ejercicio 2.2
Para este ejercicio, hay varias formas de resolver, por ejemplo, cuando el usuario seleccione un producto incorrecto, en lugar de repetir el mismo mensaje, proporcionar una aclaración u orientación para ayudarle a realizar una selección correcta. Por ejemplo, se puede mostrar información adicional sobre cada producto u ofrecer una breve descripción para ayudar al usuario a elegir el correcto.

Otra alternativa sería limitar el número de reintentos: Establecer un límite en el número de veces que el bot pedirá al usuario que seleccione un producto y cantidad. Una vez alcanzado el límite, puede ofrecer un mensaje alternativo o pedir al usuario que solicite ayuda con una persona.

En cuanto a la opción elegida fue la de limitar a una cierta cantidad de reintentos. Para ello, creé una clase donde almacena los reintentos de cada producto, y en el caso de no existir un producto, también limitarlo.

El ejercicio se encuentra en `app.product_handler` en la class `ProductHandler`

### Posible modo de uso

```python
from app.product_handler import ProductHandler

handler = ProductHandler()
product_name = read_selected_product()
product_found = False
while product_found is False:
    print("Lista de productos disponibles...")
    try:
        product_found = handler.is_product_available(product_name, 4)
        if not product_found:
            product_name = read_selected_product()
        else:
            break
    except ManyRetriesError:
        # Do something
        pass
```

## Ejercicio 3
La función `validate_discount_code` se encuentra en `app/code_handler`

### Mode de uso
```python
from app.code_handler import validate_discount_code
validate_discount_code("primavera2021")
```

## Tests
Para correr los tests solo correr el comando de pytest. Asegurarse de tener el proyecto en `PYTHONPATH`
```sh
pytest tests
```
