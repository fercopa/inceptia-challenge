_AVAILABLE_DISCOUNT_CODES = ["Primavera2021", "Verano2021", "Navidad2x1", "heladoFrozen"]
MAX_CHARS = 3


def get_difference(word1, word2):
    word_1 = set(word1)
    word_2 = set(word2)
    return word_1.symmetric_difference(word_2)


def validate_discount_code(discount_code):
    """
    Ejemplo:
    "primavera2021" deberia devolver True, ya que al compararlo:
    vs "Primavera2021" = 2 caracteres de diferencia ("p" y "P")
    vs "Verano2021" = 7 caracteres de diferencia ('i', 'n', 'o', 'm', 'V', 'p', 'v')
    vs "Navidad2x1" = 8 caracteres de diferencia ('N', 'm', '0', 'x', 'e', 'd', 'p', 'r')
    vs "heladoFrozen" = 14 caracteres de diferencia
    ('z', 'i', 'v', 'n', 'o', 'm', '2', '0', 'd', 'p', '1', 'F', 'h', 'l')
    """
    for code in _AVAILABLE_DISCOUNT_CODES:
        elems = get_difference(discount_code, code)
        if len(elems) < MAX_CHARS:
            return True
    return False
