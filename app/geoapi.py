import requests

MIN_TEMP = 28.0


class GeoAPI:
    API_KEY = "d81015613923e3e435231f2740d5610b"
    LAT = "-35.836948753554054"
    LON = "-61.870523905384076"
    ENDPOINT = "https://api.openweathermap.org/data/2.5/weather"

    @classmethod
    def is_hot_in_pehuajo(cls):
        data = cls.get_response_data()
        main = data.get("main", {}) or {}
        temp = main.get("temp", 0.) or 0.
        return temp > MIN_TEMP

    @classmethod
    def get_response_data(cls):
        params = {
            "lat": cls.LAT,
            "lon": cls.LON,
            "units": "metric",
            "appid": cls.API_KEY,
        }
        try:
            response = requests.get(cls.ENDPOINT, params=params)
            response.raise_for_status()
            return response.json()
        except (
            requests.RequestException,
            requests.ConnectionError,
            requests.HTTPError,
            requests.TooManyRedirects,
            requests.ConnectTimeout,
            requests.ReadTimeout,
            requests.Timeout,
            requests.JSONDecodeError,
        ):
            return {}
