import pandas as pd

from app.exceptions import ManyRetriesError


MAX_RETRIES = 5
_PRODUCT_DF = pd.DataFrame(
    {
        "product_name": ["Chocolate", "Granizado", "Limon", "Dulce de Leche"],
        "quantity": [3, 10, 0, 5]
    }
)


def is_product_available(product_name, quantity):
    if quantity <= 0:
        return False
    product_df = _PRODUCT_DF.copy()
    product_df["product_name"] = product_df.product_name.str.lower()
    df = product_df[
        (product_df.product_name == product_name.lower()) &
        (product_df.quantity > 0) &
        (product_df.quantity >= quantity)
    ]
    return not df.empty


class ProductHandler:
    def __init__(self):
        self.retries = 0
        self.db = _PRODUCT_DF.copy()
        self.db["product_name"] = self.db.product_name.str.lower()
        self.product_retries = None
        self.reset_retries()

    def reset_retries(self):
        self.product_retries = {key: 0 for key in self.db.product_name}
        self.product_retries["unknown"] = 0

    def is_product_available(self, product_name, quantity):
        if quantity <= 0:
            return False
        query = self.db[
            (self.db.product_name == product_name.lower()) &
            (self.db.quantity > 0) &
            (self.db.quantity >= quantity)
        ]
        self._handle_retries(product_name, query)
        return not query.empty

    def _handle_retries(self, product_name, query):
        if not query.empty:
            self.reset_retries()
            return
        self.increase_retries(product_name)
        unknown_overflow = self.product_retries.get("unknown") > MAX_RETRIES
        product_overflow = self.product_retries.get(product_name, 0) > MAX_RETRIES
        if unknown_overflow or product_overflow:
            raise ManyRetriesError(
                f"Too many tries for product {product_name}, Try again later"
            )

    def increase_retries(self, product_name):
        if product_name in self.product_retries:
            self.product_retries[product_name] += 1
        else:
            self.product_retries["unknown"] += 1
