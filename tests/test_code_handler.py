import pytest

from app.code_handler import get_difference, validate_discount_code


@pytest.mark.parametrize(
    "word, expected",
    (
        ("Primavera2021", {"p", "P"}),
        ("Verano2021", {'i', 'n', 'o', 'm', 'V', 'p', 'v'}),
        ("Navidad2x1", {'N', 'm', '0', 'x', 'e', 'd', 'p', 'r'}),
        ("heladoFrozen", {'z', 'i', 'v', 'n', 'o', 'm', '2', '0', 'd', 'p', '1', 'F', 'h', 'l'}),
    )
)
def test_get_difference(word, expected):
    assert get_difference("primavera2021", word) == expected


@pytest.mark.parametrize(
    "discount_code, expected",
    (
        ("fake", False),
        ("primavera2021", True),
        ("Navidad2x1rstmn", False),
        ("Navidad2x1", True),
    )
)
def test_validate_discount_code(discount_code, expected):
    assert validate_discount_code(discount_code) is expected
