import pytest

from app.product_handler import is_product_available, ProductHandler, MAX_RETRIES
from app.exceptions import ManyRetriesError


@pytest.mark.parametrize(
    "product_name, quantity, expected",
    (
        ("limon", 0, False),
        ("limon", 1, False),
        ("chocolate", 0, False),
        ("chocolate", 1, True),
        ("Chocolate", 2, True),
        ("granizado", 10, True),
        ("dulce de leche", 5, True),
        ("dulce de leche", 6, False),
        ("dulce de leche", -1, False),
        ("fake", 1, False),
    )
)
def test_is_product_available(product_name, quantity, expected):
    handler = ProductHandler()
    assert is_product_available(product_name, quantity) is expected
    assert handler.is_product_available(product_name, quantity) is expected


def test_is_product_available_max_retries():
    handler = ProductHandler()
    with pytest.raises(ManyRetriesError):
        for _ in range(MAX_RETRIES + 1):
            handler.is_product_available("limon", 1)
        assert handler.product_retries.get("limon") == MAX_RETRIES + 1
