import pytest
from unittest.mock import patch
from app.geoapi import requests, GeoAPI


@pytest.mark.parametrize(
    "exception",
    (
        requests.RequestException("Request error"),
        requests.ConnectionError("Connection Error"),
        requests.HTTPError("Http error"),
        requests.TooManyRedirects("Many redirects"),
        requests.ConnectTimeout("Connection time out"),
        requests.ReadTimeout("Read timeout"),
        requests.Timeout("Timeout"),
        requests.JSONDecodeError("Boom", "", 0),
    )
)
@patch("app.geoapi.requests.get")
def test_get_response_data_with_exceptions(request_get_mock, exception):
    request_get_mock.side_effect = exception
    data = GeoAPI.get_response_data()
    assert data == {}


@pytest.mark.parametrize(
    "temperature, expected",
    (
        (29, True),
        (28, False),
        (27, False),
        (0, False),
    )
)
def test_is_hot_in_pehuajo(temperature, expected):
    data = {
        "main": {
            "temp": temperature
        }
    }
    with patch.object(GeoAPI, "get_response_data", return_value=data) as mock_:
        assert GeoAPI.is_hot_in_pehuajo() is expected
        mock_.assert_called()
